# Tidy Tuesday - Week #9
### French Train Stats


See https://github.com/rfordatascience/tidytuesday for more information.

![](trips_by_route.png)

![](madrid_marseille.png)

## Code Snippet
```r
library(tidyverse)
library(lubridate)
library(ghibli)

my_font <- "Comfortaa"
my_bkgd <- "#f5f5f2"
my_theme <- theme(text = element_text(family = my_font, color = "#22211d"),
                  rect = element_rect(fill = my_bkgd),
                  plot.background = element_rect(fill = my_bkgd, color = NA),
                  panel.background = element_rect(fill = my_bkgd, color = NA),
                  panel.border = element_blank(),
                  legend.background = element_rect(fill = my_bkgd, color = NA),
                  legend.key = element_rect(fill = my_bkgd),
                  plot.caption = element_text(size = 6))

theme_set(theme_light() + my_theme)

full_trains <- read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-02-26/full_trains.csv")

# add date column
full_trains <- full_trains %>% mutate(date = ymd(paste0(year,"-",month,"-",1))) 

# create bar chart identifying routes with highest cancellation rates
hilight <- ghibli_palettes$MononokeMedium[3]

full_trains %>%
  group_by(departure_station, arrival_station) %>%
  summarize(total_cancels = sum(num_of_canceled_trains),
            total_num_trips = sum(total_num_trips),
            avg_journey_time = mean(journey_time_avg),
            marseille = if_else(departure_station == "MARSEILLE ST CHARLES" || arrival_station == "MARSEILLE ST CHARLES", T, F)) %>%
  ungroup() %>%
  mutate("perc_canceled" = 100 * total_cancels / total_num_trips,
         departure_station = str_to_title(departure_station),
         arrival_station = str_to_title(arrival_station),
         "route" = paste(departure_station,"to",arrival_station),
         route = fct_reorder(route, perc_canceled)) %>%
  top_n(10) %>%
  ggplot(aes(route,perc_canceled)) +
  geom_col(aes(fill=marseille), show.legend = F) +
  geom_text(aes(label = paste0(round(perc_canceled,1),"%")), family = my_font, col = my_bkgd, hjust=1.2) +
  scale_fill_manual(values = c(ghibli_palettes$MononokeMedium[1],hilight)) + 
  scale_y_continuous(labels = function(x) paste0(x,"%")) +
  coord_flip() + 
  geom_segment(x = 6, y = 15, xend = 8, yend = 16, arrow = arrow(angle = 30, length = unit(0.1, "inches")), col = ghibli_palettes$MononokeMedium[1]) +
  annotate("text", x = 5, y = 14, family = my_font, label = "Marseille St Charles and\nMadrid have an unusually\nhigh cancel rate") +
  labs(x ="", y="",
       title = "Percent of Canceled Trips By Route",
       subtitle = "Marseille St Charles station accounts for more than half of the top ten routes",
       caption = "Source: SNCF (National Society of French Railways)  |  Visualization by @DaveBloom11") +
  theme(plot.title = element_text(hjust = -2),
        plot.subtitle = element_text(hjust = 1.21, color = hilight, face = "bold"))

# create filtered version of full_trains
madrid_marseille <- full_trains %>%
  filter(str_detect(departure_station,"MADRID|MARSEILLE") & str_detect(arrival_station,"MADRID|MARSEILLE"))

# create line chart focusing on madrid / marseille cancels
madrid_marseille %>%
  group_by(month) %>%
  mutate(cancels = sum(num_of_canceled_trains),
         trips = sum(total_num_trips)) %>%
  ggplot(aes(date, cancels)) +
  geom_line(col = hilight, size = 2) +
  geom_point(shape = 21, fill = my_bkgd, col = hilight, size = 3) +
 geom_curve(x = as.Date("2018-02-01"), y = 32, xend = as.Date("2018-03-28"), yend = 34, 
            curvature = -0.5, angle = 75, ncp = 20, arrow = arrow(angle = 30, length = unit(0.1, "inches")), 
            col = ghibli_palettes$MononokeMedium[1]) +
  annotate("text", x = as.Date("2018-01-01"), y = 31, family = my_font, label = "The spike in number\nof cancels was\ncaused by rolling\nstock in April.\nHowever, there is\nno info on\ncauses in\nMay or June.", hjust = 0, vjust = 1) +
  scale_x_date(labels = scales::date_format(format = "%b"), breaks = "1 months") +
  labs(x = "2018", y = "",
       title = "Number of Cancels on the Madrid / Marseille St Charles Route",
       caption = "Source: SNCF (National Society of French Railways)  |  Visualization by @DaveBloom11")


```
